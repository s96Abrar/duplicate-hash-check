TEMPLATE = app
TARGET = duplicateHashCheck
VERSION = 2.0.0

QT       += widgets

CONFIG += c++11

unix {
	target.path = /usr/bin
	INSTALLS += target
}

SOURCES += \
    DuplicateCheckerGUI.cpp \
    main.cpp \
#	DuplicateCheckerGUI.cpp \
	DuplicateChecker.cpp

HEADERS += \
    DuplicateChecker.h \
 \#	DuplicateCheckerGUI.h
    DuplicateCheckerGUI.h

FORMS += \
 \#    DuplicateCheckerGUI.ui
    DuplicateCheckerGUI.ui
