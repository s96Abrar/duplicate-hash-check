/*
	Duplicate Hash Check
	Copyright (C) 2020  Abrar

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "DuplicateChecker.h"
#include <QDebug>
#include <QFileInfo>
#include <QDirIterator>
#include <QCryptographicHash>

DuplicateChecker::DuplicateChecker(const QStringList &files, bool isRecursive)
{
	listAllFiles(files, isRecursive);
}

QStringList DuplicateChecker::getFiles() const
{
	return m_files;
}

QMap<QString, QStringList> DuplicateChecker::getDuplicates() const
{
	return m_duplicates;
}

QMap<QString, QStringList> DuplicateChecker::checkDuplicate()
{
	QMap<QString, QStringList> map;
	m_duplicates.clear();

	QByteArray data;
	Q_FOREACH (QString file, m_files) {
		QFile f(file);
		f.open(QIODevice::ReadOnly);
		data = f.readAll();
		f.close();

		QString hash = QString(QCryptographicHash::hash(data, QCryptographicHash::Md5).toHex());

		if (!map[hash].empty()) {
			m_duplicates[map[hash].at(0)].append(file);
		} else {
			map[hash].append(file);
		}
	}
	return m_duplicates;
}

void DuplicateChecker::listAllFiles(const QStringList &files, bool isRecursive)
{
	Q_FOREACH (QString file, files) {
		QFileInfo fi(file);
		if (fi.isFile()) {
//			qWarning() << "Added..." << fi.absoluteFilePath();
			m_files.append(fi.absoluteFilePath());
		} else if (fi.isDir() && isRecursive) {
//			qWarning() << "Checking in..." << fi.absoluteFilePath();
			QDirIterator it(fi.absoluteFilePath(), QDir::Files | QDir::System | QDir::NoDotAndDotDot | QDir::Hidden, QDirIterator::Subdirectories);
			while (it.hasNext()) {
				it.next();
//				qWarning() << "Added..." << it.filePath();
				m_files.append(it.filePath());
			}
		}
	}
}
