/*
	Duplicate Hash Check
	Copyright (C) 2020  Abrar

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>
#include "DuplicateCheckerGUI.h"
#include "DuplicateChecker.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QApplication::setApplicationName("Duplicate Hash Checker");
	QApplication::setApplicationDisplayName("Duplicate Hash Checker");
	QApplication::setApplicationVersion("2.0.0");
	QApplication::setQuitOnLastWindowClosed(true);

	QCommandLineParser parser;
	parser.setApplicationDescription("Check md5 hash for given list of files and find the duplicate item.");
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addPositionalArgument("files", "List of files or directories");

	QCommandLineOption guiOption(QStringList() << "g" << "gui", "Open the gui for better interaction");
	QCommandLineOption recursiveOption(QStringList() << "r" << "recursive", "Take all the files within a directory");
	parser.addOption(guiOption);
	parser.addOption(recursiveOption);

	parser.process(a);

	const QStringList files = parser.positionalArguments();
	bool isGui = parser.isSet("g");
	bool isRecursive = parser.isSet("r");

	DuplicateChecker checker(files, isRecursive);
	DuplicateCheckerGUI w(&checker);

	if (isGui) {
		qInfo() << "Showing gui";
		w.show();
	} else {
		QMap<QString, QStringList> duplicates = checker.checkDuplicate();
		if (duplicates.empty()) {
			qInfo() << "All files are unique!!!";
		} else {
			qInfo() << "Duplicated files";
			Q_FOREACH(QString file, duplicates.keys()) {
				qInfo() << file;
				Q_FOREACH(QString t, duplicates[file]) {
					qInfo() << "\t" << t;
				}
				qInfo() << "";
			}
		}
		a.quit();
		return 0;
	}

	return a.exec();
}
