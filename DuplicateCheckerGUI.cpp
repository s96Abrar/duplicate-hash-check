/*
	Duplicate Hash Check
	Copyright (C) 2020  Abrar

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QDebug>
#include <QMap>

#include "DuplicateCheckerGUI.h"
#include "ui_DuplicateCheckerGUI.h"

#include "DuplicateChecker.h"

DuplicateCheckerGUI::DuplicateCheckerGUI(DuplicateChecker *checker, QWidget *parent) :
	QWidget(parent),
	ui(new Ui::DuplicateCheckerGUI),
	m_checker(checker)
{
	ui->setupUi(this);

	//  m_folderpath = QFileDialog::getExistingDirectory(this, "Select folder to check", QDir::homePath());

	//  if (!m_folderpath.count()) {
	//      qDebug() << "Nothing!";
	//      QApplication::quit();
	//  }

	//  ui->folderpath->setText(m_folderpath);
	//  connect(ui->check, &QToolButton::clicked, this, &DuplicateCheckerGUI::getHash);

	qInfo() << "Check constructor";

	QMap<QString, QStringList> duplicates = checker->checkDuplicate();

	if (duplicates.empty()) {
		ui->status->setText("All files are unique!!!");
	} else {
		ui->status->setText(QString("%1 Duplicates found").arg(duplicates.count()));
	}

	ui->files1->clear();
	Q_FOREACH (QString file, duplicates.keys()) {
		ui->files1->addItem(file);
	}

	connect(ui->files1, &QListWidget::itemClicked, this, [ = ](QListWidgetItem * item) {
		ui->files2->clear();
		Q_FOREACH (QString file, duplicates[item->text()]) {
			ui->files2->addItem(file);
		}
	});
}

DuplicateCheckerGUI::~DuplicateCheckerGUI()
{
	delete ui;
}
